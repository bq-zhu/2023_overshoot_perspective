# Notebook README

Below, we provide a brief overview of the purpose of each notebook. You will need to run through the notebooks sequentially except for the notebook `101_table1_estimates.ipynb`.

For questions related to all notebooks `1*` and `2*` please contact: gaurav.ganti@climateanalytics.org

For questions related to [TODO]

|Notebook name|Notebook description|
|-------------|--------------------|
|`101_table1_estimates.ipynb`| Estimate the net-negative emissions for Paris Agreement scenarios|
|`201_derive_tcre_wg1.ipynb` | Derive TCRE distribution (lognormal) based on IPCC AR6 WG1|
|`202_calculate_nnce.ipynb` | Estimate net-negative emissions for 0.1°C cooling using TCRE from previous notebook|
|`203_prepare_ren_emission_data.ipynb`| Adapt Ren scenario with net-negative emissions between -5 to -25 Gt CO2/yr|
|`204_prepare_fair_params.ipynb` | Prepare the FaIR parameters (AR6 WG3) to remove solar cycle forcing after 2015|
|`205_run_fair_ren.ipynb`|Run the adapated Ren scenarios through FaIR and crunch metrics|
|`206_crunch_tcre_zec_fair.ipynb`|Crunch metrics for the original Ren_NZCO2 scenario|
|`207_calculate_nnce_fair.ipnyb`|Calculate additional net-negative emissions for the Ren scenario|
|`208_check_fair_neg.ipynb`| Check the FaIR output for the "95th percentile" outcome|
|`209_figure_2.ipynb`|Plot figure 2|