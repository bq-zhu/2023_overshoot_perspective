# Helper utilities
import pyam
import pandas as pd
import numpy as np

def get_year_of_max(df):
    years = df[df==df.max()]
    return years.index[0]

def get_from_meta_column(df, x, col):
    """From: https://data.ene.iiasa.ac.at/sr15_scenario_analysis/assessment/sr15_2.0_categories_indicators.html"""
    val = df.meta.loc[x.name[0:2], col]
    return val if val < np.inf else max(x.index)

#%% Helper - construct new emission pathways
# Now that we will apply this in two different notebooks..
def construct_new_floor_pathway(df, var, floor):
    """
    Simple function to construct a new emission 
    pathway that achieves and maintains the floor
    value between 2061 and 2100
    """
    # Step 1: Filter for the variable and generate timeseries
    df_ts = (
        df
        .filter(variable=var)
        .timeseries()
    )
    # Step 2: Now change all values between 2061 and 2100 to
    # the floor value
    for t in range(2061,2101):
        df_ts.loc[:,t] = floor
    # Step 3: Create a compiled dataframe
    compiled_data = pyam.concat(
        [
            pyam.IamDataFrame(df_ts),
            df.filter(
                variable=var,
                keep=False
            )
        ]
    )
    # Step 4: Create an aggregate CO2 variable
    compiled_data.aggregate(
        variable='Emissions|CO2',
        components=[
            'Emissions|CO2|Energy and Industrial Processes',
            'Emissions|CO2|AFOLU'
        ],
        append=True
    )
    # Step 5: Rename the scenario column
    compiled_data.rename(
        scenario={
            'Ren_NZCO2':f'Ren_NZCO2_{floor*-1}'
        },
        inplace=True
    )

    # Step 6: Calculate the cumulative CO2 emissions
    co2 = (
        compiled_data
        .filter(
            variable='Emissions|CO2'
        )
        .timeseries()
    )
    cumulative = co2.apply(
        lambda x: pyam.cumulative(
            x,
            first_year=2060,
            last_year=2100
        ),
        axis=1
    ).values[0]

    # Step 7: Return the dataframe
    return compiled_data,cumulative