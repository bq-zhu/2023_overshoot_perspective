# 2023_overshoot_perspective

This repository collects the code and scripts underlying [TODO]

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This repository contains the scripts that accompany the paper
> TODO

## Data and citations

The IPCC AR6 WG III scenario data that is used in this paper can be obtained from [here](https://data.ene.iiasa.ac.at/ar6/). The data file should be placed in the `data` folder. Please cite the following:

Chapter 3 of the WG III contribution to the IPCC's 6th Assessment Report
> Riahi, K. et al. Mitigation pathways compatible with long-term goals. in IPCC, 2022: Climate Change 2022: Mitigation of Climate Change. Contribution of Working Group III to the Sixth Assessment Report of the Intergovernmental Panel on Climate Change (eds. Shukla, P. R. et al.) (Cambridge University Press, 2022). doi:10.1017/9781009157926.005.

The AR6 scenario database hosted by IIASA
> Byers, E. et al. AR6 Scenarios Database. (2022) doi:10.5281/zenodo.5886912.

You will need to place the folder `fair_temperatures` and the file `tier1_emissions.csv` from [here](https://zenodo.org/record/7194542#.ZBtCUxXMLDI) in the `data` folder.

## Note
Please refer to the `README.md` document in the `analysis` folder. It documents the content in each notebook.

## Installation
```
$ conda env create --file environment.yml
$ conda activate provide_perspective
```
